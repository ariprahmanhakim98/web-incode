<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'incode' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']|Q)LRrgf{VJB1U<rBJ.t[d(b^fQ^EDXG9%e*Y{@2l@qzU($s1E&|~ cdT_OW3`H' );
define( 'SECURE_AUTH_KEY',  ';{xowro }<ie9,j,Sr4N=f!Bbz)u*,Yuo~}Xs6vO&rF8eTr7ir4K2(p_}<w%y.w-' );
define( 'LOGGED_IN_KEY',    '>)cg2c+?ESiHj=?be!v*R2JmnNZ!KCExc-t$$qI-DBr##)cwmJ3<y2M.xjSO4#=]' );
define( 'NONCE_KEY',        'f x-c.}x!)nErbtrpjj:x58?Yici.*$q9IsI[G:X})PRVuWNPozh762$=$~fU<fm' );
define( 'AUTH_SALT',        'Io5:Rr&b&b~oe J>J%%r_{_4}n1p783QavKh#6Iw+o>fweh@&(]]n2XV?sW4%+2M' );
define( 'SECURE_AUTH_SALT', ';cz03/4O#&/26BaU*9htn.}V_p3<GqxKSQ2Pi=<+$>Fm A3%RwbiWS;tEdLs}q7_' );
define( 'LOGGED_IN_SALT',   '`2Lef~1Pl;X[WW=H%aK^b@6yx2(Boz}HRjoNJ@7e,9|}|?]gov|g`OhB*1ZKpz1U' );
define( 'NONCE_SALT',       '-.H4h)5?R:LSlF?-ECRBvQz[{s/Tzbcp0<|=7VX(sV0c>Nr`/3q[E?-}+S_Q/7O|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
